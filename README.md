## Dragon Curve
-----------
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

Draws a dragon curve on the screen based on the parameters provided.

## Usage
To run `python3 app.py` for the most basic curve with default parameters.

## Customization
Use the following optional parameters to make adjustments to the curve.
  
  -l length, --length length
  Length of the line drawn, as a number (int)  
  -s shape, --shape shape  
  Choose square or circle  
  -i iteration, --iter iteration  
  Number of iterations to complete  
  -c color, --color color  
  Hex Color with #  `Example: python3 app.py -c '#EEce4a'`
