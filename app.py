"""
*  Author: Stan Ragets
*  Email: psa119165@protonmail.com
*  Purpose: Draw a customizable Dragon Curve on the Screen
"""
from dragon import dragon, R
from turtle import Turtle, Screen
import argparse

parser = argparse.ArgumentParser(description="Make a Dragon Curve")
parser.add_argument(
    "-l",
    "--length",
    metavar="length",
    default=10,
    type=int,
    help="Length of the line drawn",
)
parser.add_argument(
    "-s",
    "--shape",
    metavar="shape",
    default="square",
    type=str,
    help="Choose square or circle",
)
parser.add_argument(
    "-i",
    "--iter",
    metavar="iteration",
    default=12,
    type=int,
    help="Number of iterations to complete",
)
parser.add_argument(
    "-c",
    "--color",
    metavar="color",
    default="#00EAD9",
    type=str,
    help="Hex Color with #",
)
args = parser.parse_args()

# Turtle Setup
turtle = Turtle("turtle")
turtle.hideturtle()
turtle.speed("fastest")
turtle.color(args.color)

# Screen Setup
screen = Screen()
screen.title("Dragon Curve")
screen.bgcolor("black")
screen.screensize(1920 * 3, 1080 * 3)
# screen.screensize(1920, 1080)
screen.setup(width=1.0, height=1.0, startx=None, starty=None)

# Draw
turtle.forward(args.length)
for element in dragon(args.iter):
    if args.shape.lower() == "square":
        if element == R:
            turtle.right(90)
            turtle.forward(args.length)
        else:
            turtle.left(90)
            turtle.forward(args.length)
    else:
        if element == R:
            turtle.circle(-4, 90, 36)
        else:
            turtle.circle(4, 90, 36)

turtle.color("white")
# turtle.write("click to exit", font=("Calibri", 16, "bold"))
screen.exitonclick()
