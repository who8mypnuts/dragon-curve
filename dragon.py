R = "R"
L = "L"


def iterate(sequence: str) -> str:
    sequence = sequence + R + swapLetters(sequence[::-1])
    return sequence


def swapLetters(sequence: str) -> str:
    newSequence = ""
    for letter in sequence:
        if letter == R:
            newSequence = newSequence + L
        else:
            newSequence = newSequence + R
    return newSequence


def dragon(n_iterations: int) -> str:
    """ Takes in a number n, and returns the dragon curve sequence
    When n=2, returns "RRL"

    Args:
        n_iterations (int): number of iterations of the dragon curve

    Returns:
        str: The dragon curve sequence
    """
    initial_sequence = R
    for _ in range(n_iterations):
        initial_sequence = iterate(initial_sequence)
    return initial_sequence
